package core;

import helpers.ConnectHelper;

import java.sql.*;
import java.util.Scanner;

public class Connect {
    String username = "root";
    String password = "";
    String database = "flight";
    String table = "test_table";
    String connectString = "jdbc:mysql://localhost:3306/";

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public Connect() {
    }
    
    public Connect(String database) {
        this("", "", database);
    }

    public Connect(String username, String password, String database) {
        this.database = database;
        this.connectString = connectString + database;
        if (username == "" && password == "") return;
        this.username = username;
        this.password = password;
    }

    // �ppnar connection till vald databas
    public Connection openConnection() {
        try {
            this.connection = DriverManager.getConnection(connectString, username, password);
            this.statement = ConnectHelper.getStatement(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
    
    // Kollar och g�r vad som beh�ver g�ras f�r att 
    // datorbasen ska funger som den ska
    public void setupDatabase() {
        try {
        	if(!ConnectHelper.doesDatabaseExist(connection, database))
        	{
        		ConnectHelper.createDatabase(connection, database);
        		ConnectHelper.useDatabase(statement, database);
        		ConnectHelper.createTables(statement);
        		ConnectHelper.insertDummyData(statement);
        	}
        	else { ConnectHelper.useDatabase(statement, database); }
        } catch (Exception e) {
            System.out.println("You need to openConnection() first!");
        }
    }

    // Basic anv�ndar query
    public ResultSet executeUserInputQuery() {
        Scanner inputScanner = new Scanner(System.in);
        String inputString;
        System.out.print("Write query: ");
        inputString = inputScanner.nextLine();
        ResultSet resultSet = executeQuery(inputString);
       
        return resultSet;
    }

    // Basic query
    public ResultSet executeQuery(String query) {
        try {
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }

    // Skriver ut det senaste ResultSet
    public void printLastResult() {
        if (resultSet == null) {
            System.out.println("Resultset is null");
            return;
        }

        try {
            int columnCount = resultSet.getMetaData().getColumnCount();
            for (int i = 1; i <= columnCount; i++) {
                if (i == columnCount) {
                    System.out.println(resultSet.getMetaData().getColumnLabel(i));
                    System.out.println("--------------------------");
                    break;
                }
                System.out.print(resultSet.getMetaData().getColumnLabel(i) + " ");
            }
            while (resultSet.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    if (i == columnCount) {
                        System.out.println(resultSet.getString(i) + " ");
                        continue;
                    }
                    System.out.print(resultSet.getString(i) + " ");
                }
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    // Tar bort databasen
    public void deleteDatabase(){
    	ConnectHelper.deleteDatabase(statement, database);
    }
    // Ger tillbaka det aktuella statementet
    public Statement getCurrentStatement(){
    	return statement;
    }
}