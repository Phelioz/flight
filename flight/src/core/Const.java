package core;

public final class Const {
    
    // Tabell namn
    public static final String AIRPORT = "airport";
    public static final String AIRLINE = "airline";
    public static final String FLIGHT = "flight";
    public static final String BOOKING = "booking";
    
	// Tabell skapelse queries
	public static final String AIRPORT_CREATION = "CREATE TABLE airport(id int AUTO_INCREMENT, "
			+ " name VARCHAR(3), fullname VARCHAR(60), location VARCHAR(60), PRIMARY KEY(id))";
	public static final String AIRLINE_CREATION = "CREATE TABLE airline(id int AUTO_INCREMENT, "
			+ "name VARCHAR(60), PRIMARY KEY(id))";
	public static final String FLIGHT_CREATION = "CREATE TABLE flight(id int AUTO_INCREMENT, "
			+ "airline VARCHAR(60), toairport VARCHAR(3), lairport VARCHAR (3), "
			+ "totime TIMESTAMP, PRIMARY KEY(id))";
	public static final String BOOKING_CREATION = "CREATE TABLE booking(id int AUTO_INCREMENT, "
			+ " fname VARCHAR(45), lname VARCHAR(45), personid VARCHAR(13), flight int, PRIMARY KEY(id))";
	
	// Menyer
	public static final String[] MAIN_MENU = {"-- Huvud Meny --","1. Kund", "2. Administration", "- 0 Avsluta -"};
	  // Kund menyn
	public static final String[] CUSTOMER_MENU = {"-- Kund Meny --","1. Boka/Se tillg�ngliga flyg", "2. F� boknings info", "- 0 f�r att g� tillbaka -"};
	public static final String[] CUSTOMER_BOOKANDSEE_MENU = {"-- Boka/Se Flyg Meny --","1. V�lja fr�n alla flyg", "2. V�lja alla flyg mellan tv� flygplatser", "- 0 f�r att g� tillbaka -"};
	  // Admin menyn
	public static final String[] ADMIN_MENU = {"-- Administrations Meny --","1. L�gg upp data", "2. Visa data", "- 0 f�r att g� tillbaka -"};
	public static final String[] ADMIN_INSERTDATA_MENU = {"-- L�gg Upp Data Meny --","1. Ny Flygplats", "2. Nytt Flygbolag", "3. Ny Flyglinje", "- 0 f�r att g� tillbaka -"};
    public static final String[] ADMIN_SHOWDATA_MENU = {"-- Visa Data Meny --","1. Visa alla flygplatser", "2. Visa alla flygbolag", "3. Visa alla flyglinjer", "4. Visa alla bokningar", "- 0 f�r att g� tillbaka -"};
	//
	
	// Tabelllernas column titlar
	public static final String AIRPORT_HEADER = "Index | ID | Namn | Fullt Namn | Stad";
	public static final String AIRLINE_HEADER = "Index | ID | Namn";
	public static final String FLIGHT_HEADER = "Index | Flygnummer | Flygbolag | Avg�ng | Destination | Datum";
	public static final String FLIGHT_HEADER_CUSTOM = "Index | Flygnummer | Flygbolag | Avg�ng | Destination | Datum";
	public static final String BOOKING_HEADER = "Index | ID | F�rnamn | Efternamn | Personnummer | Flygnummer";
	
	 // �vrigt
    public static final String CHOOSE_INDEX = "Tryck in Index siffran f�r att g�ra ditt val: ";
    public static final String LINE_DOTTED = "---------------------------------------------------------";

}
