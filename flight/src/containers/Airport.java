package containers;

public class Airport extends Container {
	private String name;
	private String fullName;
	private String location;
	
	public Airport(){}
	
	public Airport(String name, String fullname, String location){
		this.name = name;
		this.fullName = fullname;
		this.location = location;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
