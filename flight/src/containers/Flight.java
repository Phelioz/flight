package containers;

import java.sql.Timestamp;

public class Flight extends Container {
    private String airline;
    private String toAirport;
    private String lAirport;
    private String flightNummer;
    private Timestamp toTime;

    public Flight(){
    }
    public Flight(String airline, String toAirport, String lAirport, Timestamp toTime){
      this.airline = airline;
      this.toAirport = toAirport;
      this.lAirport = lAirport;
      this.toTime = toTime;
  }
    public Flight(String airline, String toAirport, String lAirport, String flightNummer, Timestamp toTime){
        this.airline = airline;
        this.toAirport = toAirport;
        this.lAirport = lAirport;
        this.flightNummer = flightNummer;
        this.toTime = toTime;
    }
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public String getToAirport() {
		return toAirport;
	}
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}
	public String getlAirport() {
		return lAirport;
	}
	public void setlAirport(String lAirport) {
		this.lAirport = lAirport;
	}
	public String getFlightNummer() {
		return flightNummer;
	}
	public void setFlightNummer(String flightNummer) {
		this.flightNummer = flightNummer;
	}
    public Timestamp getToTime() {
      return toTime;
    }
    public void setToTime(Timestamp toTime) {
      this.toTime = toTime;
    }
    public void setToTime(String year, String month, String date, String hour, String minute) {
      @SuppressWarnings("deprecation")
      Timestamp timestamp = new Timestamp(Integer.parseInt(year), Integer.parseInt(month), 
          Integer.parseInt(date), Integer.parseInt(hour), Integer.parseInt(minute), 0, 0);
      this.toTime = timestamp;
    }

}
