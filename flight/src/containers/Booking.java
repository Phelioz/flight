package containers;

public class Booking extends Container {
	private String fname;
	private String lname;
	private String personid;
	private String flight;
	
	public Booking(){}
	
	public Booking(String fname, String lname, String personid, String flight){
		this.fname = fname;
		this.lname = lname;
		this.personid = personid;
		this.flight = flight;
	}
	
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getPersonid() {
		return personid;
	}
	public void setPersonid(String personid) {
		this.personid = personid;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}
}
