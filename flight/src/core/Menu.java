package core;

import helpers.ContainerHelper;
import helpers.MiscHelper;
import static helpers.MiscHelper.isDigit;
import static helpers.MiscHelper.isWord;
import static helpers.MiscHelper.isPersonID;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Scanner;

import containers.Airline;
import containers.Airport;
import containers.Booking;
import containers.Flight;

public class Menu {
    private Connect connect;
    private Scanner inputScanner;
    private String inputString;

    public Menu(){
    }
    
    // Initialiserar allt som beh�vs f�r att menyn ska fungera
    public void init(){
        connect = new Connect();
        connect.openConnection();
//        connect.deleteDatabase();
        connect.setupDatabase();
        mainMenu();
    }
    
    private void mainMenu(){
        inputScanner = new Scanner(System.in);
        System.out.println(Const.LINE_DOTTED);
        outerloop:
        while (true){
            for (String item : Const.MAIN_MENU) {
              System.out.println(item);
            }
            inputString = inputScanner.next();
            System.out.println(Const.LINE_DOTTED);
            switch (inputString) {
              case "0":
                inputScanner.close();
                break outerloop;
                
              case "1":
                customerMenu();
                break;
                
              case "2":
                adminMenu();
                break;

              default:
                break;
            }
        }
    }
 
    // Kund menyn startar h�r  
    private void customerMenu(){
      inputScanner = new Scanner(System.in);
      System.out.println(Const.LINE_DOTTED);
      outerloop:
      while (true) {
        for (String item : Const.CUSTOMER_MENU) {
          System.out.println(item);
        }
        inputString = inputScanner.next();
        System.out.println(Const.LINE_DOTTED);
        switch (inputString) {
          case "1":
            bookAndSeeFlights();
            break;
          
          case "2":
            getBookingInfo();
            break;

          default:
            break outerloop;
        }
      }
    }
 
    // Boka/Se tillg�ngliga flyg menyn start h�r    
    private void bookAndSeeFlights(){
      inputScanner = new Scanner(System.in);
      System.out.println(Const.LINE_DOTTED);
      outerloop:
      while (true) {
        for (String item : Const.CUSTOMER_BOOKANDSEE_MENU) {
          System.out.println(item);
        }
        inputString = inputScanner.next();
        System.out.println(Const.LINE_DOTTED);
        switch (inputString) {
          case "1":
            bookFromAll();
            break;
          
          case "2":
            bookFromSelected();
            break;

          default:
            break outerloop;
        }
      }
    }
    
    private void bookFromAll(){
      inputScanner = new Scanner(System.in);
      Booking booking = new Booking();
      String flightNum = "";
      try {
       while (true) {
          ArrayList<String> flightIDs = ContainerHelper.showTable(
              connect.getCurrentStatement(), Const.FLIGHT);
          flightNum = MiscHelper.idFromIndex(flightIDs);
          if (flightNum == null) return;
            break;
          
       } 
       booking = bookingWithoutFlight();
       booking.setFlight(flightNum);
       ContainerHelper.uploadContainer(connect.getCurrentStatement(), booking);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    
    private void bookFromSelected(){
      inputScanner = new Scanner(System.in);
      Booking booking = new Booking();
      String flightNum = "";
      String tempString = "";
      String toAirport = "";
      String lAirport = "";
      try {
       while (true) {
         ArrayList<String> airportIDs = ContainerHelper.showTable(
             connect.getCurrentStatement(), Const.AIRPORT);
         System.out.println("Vart du ska flyga ifr�n");
         tempString = MiscHelper.idFromIndex(airportIDs);
         if(tempString == null) return;
         toAirport = ContainerHelper.getChoosenTableNameColumnById(connect.getCurrentStatement(), Const.AIRPORT, tempString);
         
         airportIDs = ContainerHelper.showTableWhereNameIsNot(
             connect.getCurrentStatement(), Const.AIRPORT, toAirport);
         System.out.println("Vart du ska flyga till");
         tempString = MiscHelper.idFromIndex(airportIDs);
         if(tempString == null) return;
         lAirport = ContainerHelper.getChoosenTableNameColumnById(connect.getCurrentStatement(), Const.AIRPORT, tempString);
         
         ArrayList<String> flightIDs = ContainerHelper.showFlightsWhere(connect.getCurrentStatement(), toAirport, lAirport);
         flightNum = MiscHelper.idFromIndex(flightIDs);
         if (flightNum != null){
           break;
         }
         else return;
       } 
       booking = bookingWithoutFlight();
       booking.setFlight(flightNum);
       ContainerHelper.uploadContainer(connect.getCurrentStatement(), booking);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    
    private Booking bookingWithoutFlight(){
      inputScanner = new Scanner(System.in);
      Booking booking = new Booking();
      while (true) {
        System.out.print("Skriv in ditt f�rnamn: ");
        inputString = inputScanner.next();
        booking.setFname(inputString);
        if(isWord(inputString)) break;
        System.out.println("Bara bokst�ver �r accepterade, testa igen");
      }
      while (true) {
        System.out.print("Skriv in ditt efternamn: ");
        inputString = inputScanner.next();
        booking.setLname(inputString);
        if(isWord(inputString)) break;
        System.out.println("Bara bokst�ver �r accepterade, testa igen");
      }
      while (true) {
        System.out.print("Skriv in ditt personnummer(12 siffror): ");
        inputString = inputScanner.next();
        if(isPersonID(inputString)) {
          booking.setPersonid(MiscHelper.personIDformat(inputString));
          break;
        }
        System.out.println("M�ste vara 12 siffror, testa igen");
      }
      System.out.println(Const.LINE_DOTTED);
      
      return booking;
    }
    // Boka/Se tillg�ngliga flyg menyn slutar h�r  
    
    // Trycker ut kundens valda biljett info
    private void getBookingInfo(){
      inputScanner = new Scanner(System.in);
      ArrayList<String> bookingIDs = new ArrayList<String>();
      String personID = "";
      String bookingID = "";
      while(true){
        System.out.println("Skriv in ditt person nummer(12 siffror): ");
        inputString = inputScanner.next();
        if(MiscHelper.isPersonID(inputString)) break;
        System.out.println("M�ste vara 12 siffror, testa igen");
      }
      personID = MiscHelper.personIDformat(inputString);
      try {
        bookingIDs = ContainerHelper.showBookingWherePersonID(connect.getCurrentStatement(), personID);
        bookingID = MiscHelper.idFromIndex(bookingIDs);
        if(bookingID == null) return;
        ContainerHelper.printTicket(connect.getCurrentStatement(), bookingID);
      } catch (SQLException e) {
        e.printStackTrace();
      }
      
    }
    // Kund menyn slutar h�r 
    
    // Admin menyn b�rjar h�r  
    private void adminMenu(){
      inputScanner = new Scanner(System.in);
      System.out.println(Const.LINE_DOTTED);
      outerloop:
      while (true) {
        for (String item : Const.ADMIN_MENU) {
          System.out.println(item);
        }
        inputString = inputScanner.next();
        System.out.println(Const.LINE_DOTTED);
        switch (inputString) {
          case "1":
            insertDataMenu();
            break;
          
          case "2":
            showDataMenu();
            break;

          default:
            break outerloop;
        }
      }
      
    }
   
    // L�gg upp data menyn b�rjar h�r 
    private void insertDataMenu(){
      inputScanner = new Scanner(System.in);
      System.out.println(Const.LINE_DOTTED);
      outerloop:
      while (true) {
        for (String item : Const.ADMIN_INSERTDATA_MENU) {
          System.out.println(item);
        }
        inputString = inputScanner.next();
        System.out.println(Const.LINE_DOTTED);
        switch (inputString) {
          case "1":
            insertAirportMenu();
            break;
            
          case "2":
            insertAirlineMenu();
            break;

          case "3":
            insertFlightMenu();
            break;

          default:
            break outerloop;
        }
      }
      
    }
   
    private void insertAirportMenu() {
      inputScanner = new Scanner(System.in);
      Airport airport = new Airport();
      System.out.print("Skriv in det f�r kortade namnet f�r flygplatsen(3 bokst�ver): ");
      inputString = inputScanner.nextLine();
      airport.setName(inputString);
      System.out.print("Skriv in det fulla namnet f�r flygplatsen: ");
      inputString = inputScanner.nextLine();
      airport.setFullName(inputString);
      System.out.print("Skriv in vilken stad flygplatsen ligger i: ");
      inputString = inputScanner.nextLine();
      airport.setLocation(inputString);
      
      try {
        ContainerHelper.uploadContainer(connect.getCurrentStatement(), airport);
      } catch (SQLException e) {
        e.printStackTrace();
      }
      
    }
   
    private void insertAirlineMenu() {
      inputScanner = new Scanner(System.in);
      Airline airline = new Airline();
      System.out.print("Skriv in namnet p� flygbolaget du vill registrera: ");
      inputString = inputScanner.next();
      airline.setName(inputString);
      
      try {
        ContainerHelper.uploadContainer(connect.getCurrentStatement(), airline);
      } catch (SQLException e) {
        e.printStackTrace();
      }
      
    }
    
    private void insertFlightMenu() {
      inputScanner = new Scanner(System.in);
      ArrayList<String> idArr = new ArrayList<String>();
      Flight flight = new Flight();
      String toAirport = "";
      try {
      while (true) {
        idArr = ContainerHelper.showTable(connect.getCurrentStatement(), Const.AIRLINE);
        System.out.println(Const.LINE_DOTTED);
        System.out.println("Vilket flygbolag ska flyga linjen");
        System.out.print(Const.CHOOSE_INDEX);
        inputString = inputScanner.next();
        if(isDigit(inputString)){
          String id = idArr.get(Integer.parseInt(inputString) - 1);
          flight.setAirline(ContainerHelper.getChoosenTableNameColumnById(connect.getCurrentStatement(), Const.AIRLINE, id));
          break;
        }
      }
      while (true) {
        idArr = ContainerHelper.showTable(connect.getCurrentStatement(), Const.AIRPORT);
        System.out.println(Const.LINE_DOTTED);
        System.out.println("Vart ska flyget g� ifr�n");
        System.out.print(Const.CHOOSE_INDEX);
        inputString = inputScanner.next();
        if(isDigit(inputString)){
          String id = idArr.get(Integer.parseInt(inputString) - 1);
          toAirport = ContainerHelper.getChoosenTableNameColumnById(connect.getCurrentStatement(), Const.AIRPORT, id);
          flight.setToAirport(ContainerHelper.getChoosenTableNameColumnById(connect.getCurrentStatement(), Const.AIRPORT, id));
          break;
        }
      }
      while (true) {
        idArr = ContainerHelper.showTableWhereNameIsNot(connect.getCurrentStatement(), Const.AIRPORT, toAirport);
        System.out.println(Const.LINE_DOTTED);
        System.out.println("Vart ska flyget g� till");
        System.out.print(Const.CHOOSE_INDEX);
        inputString = inputScanner.next();
        if(isDigit(inputString)){
          String id = idArr.get(Integer.parseInt(inputString) - 1);
          flight.setlAirport(ContainerHelper.getChoosenTableNameColumnById(connect.getCurrentStatement(), Const.AIRPORT, id));
          break;
        }
      }
      while (true) {
        System.out.println(Const.LINE_DOTTED);
        System.out.println("N�r g�r flyget");
        System.out.print("Skriv in i det s� h�r(YY-MM-DD-HH-MI) g�r ocks� bra utan bindestrecken: ");
        inputString = inputScanner.next();
        if(MiscHelper.isTimestamp(inputString)){
          Timestamp timestamp = MiscHelper.makeTimeStamp(inputString);
          flight.setToTime(timestamp);
          if(!MiscHelper.doesDateExist(connect.getCurrentStatement(), timestamp)) break;
          System.out.println("Datumet du skriv in har redan ett flyg bokat, testa ett annat datum");
        }
        else System.out.println("Du skrev in n�r flyget g�r fel, testa igen");
      }
      ContainerHelper.uploadContainer(connect.getCurrentStatement(), flight);
      System.out.println(Const.LINE_DOTTED);
      } catch (SQLException e) {
        e.printStackTrace();
      }
      
    }
    // L�gg upp data menyn slutar h�r 
    
    // Visa data menyn b�rjar h�r
    private void showDataMenu()
    {
      inputScanner = new Scanner(System.in);
      System.out.println(Const.LINE_DOTTED);
      outerloop:
      while (true) {
        for (String item : Const.ADMIN_SHOWDATA_MENU) {
          System.out.println(item);
        }
        inputString = inputScanner.next();
        System.out.println(Const.LINE_DOTTED);
        switch (inputString) {
          case "1":
            showAllAirports();
            break;
            
          case "2":
            showAllAirlines();
            break;

          case "3":
            showAllFlights();
            break;
          case "4":
            showAllBookings();
            break;

          default:
            break outerloop;
        }
      }
      
    }
    
    private void showAllAirports() {
      try {
        ContainerHelper.showTable(connect.getCurrentStatement(), Const.AIRPORT);
        System.out.println(Const.LINE_DOTTED);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    private void showAllAirlines() {
      try {
        ArrayList<String> airlineIDs = new ArrayList<String>();
        airlineIDs = ContainerHelper.showTable(connect.getCurrentStatement(), Const.AIRLINE);
        System.out.println(Const.LINE_DOTTED);
        System.out.println("V�lj ett flygbolag f�r att se vilka flyglinjer dem flyger");
        String id = MiscHelper.idFromIndex(airlineIDs);
        if(id == null) return;
        String airlineName = ContainerHelper.getChoosenTableNameColumnById(connect.getCurrentStatement(), Const.AIRLINE, id);
        ContainerHelper.printAirlineFlights(connect.getCurrentStatement(), airlineName);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    private void showAllFlights() {
      try {
        ContainerHelper.showTable(connect.getCurrentStatement(), Const.FLIGHT);
        System.out.println(Const.LINE_DOTTED);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    private void showAllBookings() {
      try {
        ContainerHelper.showTable(connect.getCurrentStatement(), Const.BOOKING);
        System.out.println(Const.LINE_DOTTED);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    // Visa data menyn slutar h�r
    // Admin menyn slutar h�r  
 

}
