package helpers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import containers.Airline;
import containers.Airport;
import containers.Booking;
import containers.Container;
import containers.Flight;
import core.Const;

public class ContainerHelper {

  // Visar vald tabell
  public static ArrayList<String> showTable(Statement statement, String tableName) throws SQLException {
    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + tableName);
    if (resultSet == null) {
      System.out.println("ResultSet is null");
      return null;
    }
    ArrayList<String> indexArray = new ArrayList<String>();
    int columnCount = resultSet.getMetaData().getColumnCount();
    switch (tableName) {
      case Const.AIRPORT:
        System.out.println(Const.AIRPORT_HEADER);
        break;
      case Const.AIRLINE:
        System.out.println(Const.AIRLINE_HEADER);
        break;
      case Const.FLIGHT:
        System.out.println(Const.FLIGHT_HEADER);
        break;
      case Const.BOOKING:
        System.out.println(Const.BOOKING_HEADER);
        break;

      default:
        break;
    }
    System.out.println(Const.LINE_DOTTED);
    while (resultSet.next()) {
      for (int i = 1; i <= columnCount; i++) {
        if(i == 1){
          indexArray.add(resultSet.getString(i));
          System.out.print(resultSet.getRow() + ".    | " + resultSet.getString(i) + " | ");
        }
        else if (i == columnCount) {
          System.out.println(resultSet.getString(i) + " | ");
          continue;
        }
        else System.out.print(resultSet.getString(i) + " | ");
      }

    }
    return indexArray;
  }

  // Visar allt fr�n vald tabell d�r id st�mmer med det valda
  public static ArrayList<String> showTableWhereID(Statement statement, String tableName, String id) throws SQLException {
    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + tableName + " WHERE id = '" + id + "';");
    if (resultSet == null) {
      System.out.println("ResultSet is null");
      return null;
    }
    ArrayList<String> indexArray = new ArrayList<String>();
    int columnCount = resultSet.getMetaData().getColumnCount();
    switch (tableName) {
      case Const.AIRPORT:
        System.out.println(Const.AIRPORT_HEADER);
        break;
      case Const.AIRLINE:
        System.out.println(Const.AIRLINE_HEADER);
        break;
      case Const.FLIGHT:
        System.out.println(Const.FLIGHT_HEADER);
        break;
      case Const.BOOKING:
        System.out.println(Const.BOOKING_HEADER);
        break;

      default:
        break;
    }
    System.out.println(Const.LINE_DOTTED);
    while (resultSet.next()) {
      for (int i = 1; i <= columnCount; i++) {
        if(i == 1){
          indexArray.add(resultSet.getString(i));
          System.out.print(resultSet.getRow() + ".    | " + resultSet.getString(i) + " | ");
        }
        else if (i == columnCount) {
          System.out.println(resultSet.getString(i) + " | ");
          continue;
        }
        else System.out.print(resultSet.getString(i) + " | ");
      }

    }
    return indexArray;
  }
  
  //Visar allt fr�n vald tabell utom valda namnet
   public static ArrayList<String> showTableWhereNameIsNot(Statement statement, String tableName, String name) throws SQLException {
     ResultSet resultSet = statement.executeQuery("SELECT * FROM " + tableName + " WHERE name != '" + name + "';");
     if (resultSet == null) {
       System.out.println("ResultSet is null");
       return null;
     }
     ArrayList<String> indexArray = new ArrayList<String>();
     int columnCount = resultSet.getMetaData().getColumnCount();
     switch (tableName) {
       case Const.AIRPORT:
         System.out.println(Const.AIRPORT_HEADER);
         break;
       case Const.AIRLINE:
         System.out.println(Const.AIRLINE_HEADER);
         break;
       case Const.FLIGHT:
         System.out.println(Const.FLIGHT_HEADER);
         break;
       case Const.BOOKING:
         System.out.println(Const.BOOKING_HEADER);
         break;
  
       default:
         break;
     }
     System.out.println(Const.LINE_DOTTED);
     while (resultSet.next()) {
       for (int i = 1; i <= columnCount; i++) {
         if(i == 1){
           indexArray.add(resultSet.getString(i));
           System.out.print(resultSet.getRow() + ".    | " + resultSet.getString(i) + " | ");
         }
         else if (i == columnCount) {
           System.out.println(resultSet.getString(i) + " | ");
           continue;
         }
         else System.out.print(resultSet.getString(i) + " | ");
       }
  
     }
     return indexArray;
   }
  
  // Visar alla bokningar d�r personnummret st�mmer med det valda
  public static ArrayList<String> showBookingWherePersonID(Statement statement, String personID) throws SQLException {
    ResultSet resultSet = statement.executeQuery("SELECT * FROM booking WHERE personid = '" + personID + "';");
    if (resultSet == null) {
      System.out.println("ResultSet is null");
      return null;
    }
    ArrayList<String> indexArray = new ArrayList<String>();
    int columnCount = resultSet.getMetaData().getColumnCount();
    
    System.out.println(Const.BOOKING_HEADER);
    System.out.println(Const.LINE_DOTTED);
    
    while (resultSet.next()) {
      for (int i = 1; i <= columnCount; i++) {
        if(i == 1){
          indexArray.add(resultSet.getString(i));
          System.out.print(resultSet.getRow() + ".    | " + resultSet.getString(i) + " | ");
        }
        else if (i == columnCount) {
          System.out.println(resultSet.getString(i) + " | ");
          continue;
        }
        else System.out.print(resultSet.getString(i) + " | ");
      }

    }
    return indexArray;
  }

  // Skriver ut biljetten f�r det valda bokningsnumret
  public static void printTicket(Statement statement, String bookingID) throws SQLException {
    ResultSet resultSet = statement.executeQuery("SELECT * FROM booking WHERE id = '" + bookingID + "';");
    if (resultSet == null) {
      System.out.println("ResultSet is null");
      return;
    }
    System.out.println("- Din Biljett -");
    System.out.println(Const.LINE_DOTTED);
    while (resultSet.next()) {
        System.out.println("Ditt unika biljett nummer: " + resultSet.getString(1) );
        System.out.println("Flygets nummer du ska �ka med: " + resultSet.getString(5) );
        System.out.println("Person info");
        System.out.println("- Ditt f�rnamn: " + resultSet.getString(2) );
        System.out.println("- Ditt efternamn: " + resultSet.getString(3) );
        System.out.println("- Ditt personnummer: " + resultSet.getString(4) );
      }
    System.out.println(Const.LINE_DOTTED);
  }
  
  // Visar alla flyg som st�mmer med avg�ngsflygplatsen och destinationsflygplatsen
  public static ArrayList<String> showFlightsWhere(Statement statement, String toAirport, String lAirport) throws SQLException {
    ResultSet resultSet = statement.executeQuery("SELECT * FROM flight WHERE (toairport = '" + toAirport + 
        "') AND (lairport = '" + lAirport + "');");
    if (resultSet == null) {
      System.out.println("ResultSet is null");
      return null;
    }
    ArrayList<String> indexArray = new ArrayList<String>();
    int columnCount = resultSet.getMetaData().getColumnCount();
    System.out.println(Const.FLIGHT_HEADER_CUSTOM);
    System.out.println(Const.LINE_DOTTED);
    while (resultSet.next()) {
      for (int i = 1; i <= columnCount; i++) {
        if(i == 1){
          indexArray.add(resultSet.getString(i));
          System.out.print(resultSet.getRow() + ".    | " + resultSet.getString(i) + " | ");
        }
        else if (i == columnCount) {
          System.out.println(resultSet.getString(i) + " | ");
          continue;
        }
        else System.out.print(resultSet.getString(i) + " | ");
      }

    }
    return indexArray;
  }

  // Ger tillbaka namnet fr�n namn kolumnen fr�n den valda tabellen d�r id st�mmer med det valda
  public static String getChoosenTableNameColumnById(Statement statement, String tableName, String id) throws SQLException{
    ResultSet resultSet = statement.executeQuery("SELECT * FROM " + tableName +" WHERE id = '" + id + "';");
    resultSet.next();
    return resultSet.getString("name");
  }

  // Ger tillbaka alla flyglinje nummer som det valda flygbolaget flyger
  public static ArrayList<String> getAirlineFlights(Statement statement, String airlineName) throws SQLException{
    ResultSet resultSet = statement.executeQuery("SELECT id FROM flight WHERE airline = '" + airlineName + "';");
    ArrayList<String> flights = new ArrayList<String>();
    while (resultSet.next()) {
      flights.add(resultSet.getString(1));
    }
    return flights;
  }

  // Skriver ut alla flyg som det valda flygbolaget flyger
  public static void printAirlineFlights(Statement statement, String airlineName) throws SQLException{
    ResultSet resultSet = statement.executeQuery("SELECT * FROM flight WHERE airline = '" + airlineName + "';");
    int columnCount = resultSet.getMetaData().getColumnCount();
    System.out.println("- Flygbolagets Flyglinjer -");
    System.out.println(Const.LINE_DOTTED);
    while (resultSet.next()) {
      for (int i = 1; i <= columnCount; i++) {
        if(i == 1){
          System.out.print(resultSet.getRow() + ".    | " + resultSet.getString(i) + " | ");
        }
        else if (i == columnCount) {
          System.out.println(resultSet.getString(i) + " | ");
          continue;
        }
        else System.out.print(resultSet.getString(i) + " | ");
      }
    }
    System.out.println(Const.LINE_DOTTED);
  }
  
  // Laddar alla typer av container objekt upp till databasen
  public static void uploadContainer(Statement statement, Container thingToUpload)
      throws SQLException {
    if (thingToUpload instanceof Airport)
      statement.executeUpdate(airportToInsert((Airport) thingToUpload));
    if (thingToUpload instanceof Airline)
      statement.executeUpdate(airlineToInsert((Airline) thingToUpload));
    if (thingToUpload instanceof Flight)
      statement.executeUpdate(flightToInsert((Flight) thingToUpload));
    if (thingToUpload instanceof Booking)
      statement.executeUpdate(bookingToInsert((Booking) thingToUpload));
  }

  // Fr�n flygplats objekt till MySQL query
  public static String airportToInsert(Airport airport) {
    return "INSERT INTO airport (name, fullname, location) " + "VALUES ('" + airport.getName()
        + "', '" + airport.getFullName() + "', '" + airport.getLocation() + "');";
  }

  // Fr�n flygbolag objekt till MySQL query
  public static String airlineToInsert(Airline airline) {
    return "INSERT INTO airline (name) " + "VALUES ('" + airline.getName() + "');";
  }
 
  // Fr�n flyglinje objekt till MySQL query
  public static String flightToInsert(Flight flight) {
    return "INSERT INTO flight (airline, toairport, lairport, totime) " + "VALUES ('" + flight.getAirline()
        + "', '" + flight.getToAirport() + "', '" + flight.getlAirport() + "', '" + flight.getToTime().toString() + "');";
  }
  
  // Fr�n boknings objekt till MySQL query
  public static String bookingToInsert(Booking booking) {
    return "INSERT INTO booking (fname, lname, personid, flight) " + "VALUES ('"
        + booking.getFname() + "', '" + booking.getLname() + "', '" + booking.getPersonid()
        + "', '" + booking.getFlight() + "');";
  }
}
