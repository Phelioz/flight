package helpers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import containers.Airline;
import containers.Airport;
import containers.Booking;
import containers.Flight;
import core.Const;

public class ConnectHelper {

    // Checkar om databasen redan existerar
    public static boolean doesDatabaseExist(Connection connection, String databaseName) {
    	try {
    	ResultSet catalogs = connection.getMetaData().getCatalogs();
			while (catalogs.next()) {
				int count = catalogs.getMetaData().getColumnCount();
				for (int i = 1; i <= count; i++) {
					if (catalogs.getString(i).equals(databaseName)) {
						return true;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
    // B�rjar anv�nda vald databas
    public static void useDatabase(Statement statement, String databaseName){
      try {
        statement.executeUpdate("USE " + databaseName);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    
    // Skapar vald databas
	public static void createDatabase(Connection connection, String databaseName){
    	Statement tempState = getStatement(connection);
    	try {
			tempState.executeUpdate("CREATE DATABASE " + databaseName);
			tempState.executeUpdate("USE " + databaseName);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
    }
	
	// Tar bort vald databas
	public static void deleteDatabase(Statement statement, String databaseName){
	        try {
	            statement.executeUpdate("DROP DATABASE " + databaseName);
	        } catch (SQLException e) {
	        
	            e.printStackTrace();
	        }
	 }
	
	// Skapar dem tabellerna som beh�vs
	public static void createTables(Statement statement){
    	try {
			statement.executeUpdate(Const.AIRPORT_CREATION);
			statement.executeUpdate(Const.AIRLINE_CREATION);
			statement.executeUpdate(Const.FLIGHT_CREATION);
			statement.executeUpdate(Const.BOOKING_CREATION);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
    }
	  
    // Trycker in lite data i databasen s� det finns
    // n�got att testa imot
    public static void insertDummyData(Statement statement) throws SQLException{
      // Flygplatser
      ArrayList<Airport> airportArr = new ArrayList<Airport>();
      airportArr.add(new Airport("ARL", "Arlanda Airport", "Stockholm"));
      airportArr.add(new Airport("GOT", "Landvetter Airport", "G�teborg"));
      airportArr.add(new Airport("OSD", "�re �stersund Airport", "�re �sterund"));
      for (Airport airport : airportArr) {
        ContainerHelper.uploadContainer(statement
            , airport);
      }
      // Flygbolag
      ArrayList<Airline> airlineArr = new ArrayList<Airline>();
      airlineArr.add(new Airline("Scandinavian Airlines"));
      airlineArr.add(new Airline("Malm� Aviation"));
      airlineArr.add(new Airline("J�mtlands Flyg"));
      airlineArr.add(new Airline("Norwegian"));
      for (Airline airline : airlineArr) {
        ContainerHelper.uploadContainer(statement
            , airline);
      }
      // Flyglinjer
      ArrayList<Flight> flightArr = new ArrayList<Flight>();
      flightArr.add(new Flight("J�mtlands Flyg", "ARL", "OSD", MiscHelper.makeTimeStamp("15-01-09 15:11")));
      flightArr.add(new Flight("J�mtlands Flyg", "OSD", "ARL", MiscHelper.makeTimeStamp("15-01-10 12:45")));
      flightArr.add(new Flight("Scandinavian Airlines", "ARL", "GOT", MiscHelper.makeTimeStamp("15-01-11 08:30")));
      flightArr.add(new Flight("Norwegian", "GOT", "ARL", MiscHelper.makeTimeStamp("15-01-12 11:15")));
      flightArr.add(new Flight("Scandinavian Airlines", "GOT", "OSD", MiscHelper.makeTimeStamp("15-01-13 17:18")));
      flightArr.add(new Flight("Malm� Aviation", "OSD", "GOT", MiscHelper.makeTimeStamp("15-01-13 17:18")));
      for (Flight flight : flightArr) {
        ContainerHelper.uploadContainer(statement
            , flight);
      }
      // Bokningar
      ArrayList<Booking> bookingArr = new ArrayList<Booking>();
      bookingArr.add(new Booking("Sara", "Persson", "19760902-1264", "6"));
      bookingArr.add(new Booking("Rikard", "Stenlund", "19880415-5421", "4"));
      bookingArr.add(new Booking("Muhammed", "Ergebe", "19901203-4217", "4"));
      bookingArr.add(new Booking("Josefine", "Tallgren", "19580724-8533", "1"));
      bookingArr.add(new Booking("Kaleb", "Seifu", "19712807-7742", "3"));
      for (Booking booking : bookingArr) {
        ContainerHelper.uploadContainer(statement
            , booking);
      }
    }
    
	// F�r ut ett statement av connectionen
    public static Statement getStatement(Connection connection) {
       Statement statement = null;
    	if (connection == null) {
            System.out.println("Must openConnection() first");
            return null;
        }
            try {
                statement = connection.createStatement();
            } catch (SQLException e) {

                e.printStackTrace();
            }
        return statement;
    }
}
