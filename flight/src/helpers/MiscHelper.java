package helpers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import core.Const;

public class MiscHelper {
  
  // Ger ut id fr�n tabllen genom vald index
  public static String idFromIndex(ArrayList<String> getFromArr)
  {
    Scanner inputScanner = new Scanner(System.in);
    System.out.println(Const.LINE_DOTTED);
    System.out.println("(0 f�r att g� tillbaka)");
    System.out.print(Const.CHOOSE_INDEX);
    
    String inputString = inputScanner.next();
    System.out.println(Const.LINE_DOTTED);
    String idString = null;
    if(!isDigit(inputString)){ return null;}
    int index = Integer.parseInt(inputString);
    if(index <= 0) { return null;}
    if (index <= getFromArr.size() && index > 0){
      idString = getFromArr.get(index - 1);
    }
    return idString;
  }
 
  // Checkar om det �r bara siffror
  public static boolean isDigit(String stringToCheck)
  {
    Pattern pattern = Pattern.compile("^[0-9]+$");
    Matcher matcher = pattern.matcher(stringToCheck);
    if(matcher.find()) return true;
    return false;
  }
  
  // Checkar om det �r bara bokst�ver
  public static boolean isWord(String stringToCheck)
  {
    Pattern pattern = Pattern.compile("^[a-�A-�]+$");
    Matcher matcher = pattern.matcher(stringToCheck);
    if(matcher.find()) return true;
    return false;
  }
  
  // Checkar om det �r ett personnummer
  public static boolean isPersonID(String stringToCheck)
  {
    //Pattern pattern = Pattern.compile("(\\d{2})?\\d{6}.?\\d{4}");
    Pattern pattern = Pattern.compile("\\d{8}.?\\d{4}");
    Matcher matcher = pattern.matcher(stringToCheck);
    if(matcher.find()) return true;
    return false;
  }
  
  // Formaterar om en string till r�tt personnummer format f�r databasen
  public static String personIDformat(String stringToCheck)
  {
    StringBuilder strBuilder = new StringBuilder();
    Pattern pattern = Pattern.compile("(\\d{8}).?(\\d{4})");
    Matcher matcher = pattern.matcher(stringToCheck);
    matcher.find();
    strBuilder.append(matcher.group(1));
    strBuilder.append("-");
    strBuilder.append(matcher.group(2));
    return strBuilder.toString();
  }
  
  // G�r ett Timestamp objekt fr�n en string
  public static Timestamp makeTimeStamp(String input)
  {
    Pattern pattern = Pattern.compile("(\\d{2}).?(\\d{2}).?(\\d{2}).?(\\d{2}).?(\\d{2})");
    Matcher matcher = pattern.matcher(input);
    int y = 0;
    int m = 0;
    int d = 0;
    int h = 0;
    int mi = 0;
    while (matcher.find()) {
      y = Integer.parseInt(matcher.group(1));
      m = Integer.parseInt(matcher.group(2));
      d = Integer.parseInt(matcher.group(3));
      h = Integer.parseInt(matcher.group(4));
      mi = Integer.parseInt(matcher.group(5));
     }
    @SuppressWarnings("deprecation")
    // Konstigt nog verkar m�naden p� timestampen l�gg p� 1 s� d�rf�r tar jag minus 1.
    // hittade tyv�rr inget svar p� varf�r den g�r det. Den �r i f�rsig deprecated s�
    // borde inte nog egentligen anv�nda den men den passade verkligen f�r mitt syfte.
    Timestamp timestamp = new Timestamp(y + 100, m - 1, d, h, mi, 0, 0);
    return timestamp;
    
  }
  
  // Checkar om stringen �r Timestamp
  public static boolean isTimestamp(String input)
  {
    Pattern pattern = Pattern.compile("(\\d{2}).?(\\d{2}).?(\\d{2}).?(\\d{2}).?(\\d{2})");
    Matcher matcher = pattern.matcher(input);
    boolean isTimestamp = true;
    if(!matcher.find()) isTimestamp = false;
    else{
      if(matcher.groupCount() < 5) isTimestamp = false;
      int y = 0;
      int m = 0;
      int d = 0;
      int h = 0;
      int mi = 0;
      y = Integer.parseInt(matcher.group(1));
      m = Integer.parseInt(matcher.group(2));
      d = Integer.parseInt(matcher.group(3));
      h = Integer.parseInt(matcher.group(4));
      mi = Integer.parseInt(matcher.group(5));
      if(y < 0 || y > 37) isTimestamp = false;
      if(m < 1 || m > 12) isTimestamp = false;
      if(d < 1 || d > 31) isTimestamp = false;
      if(h < 0 || h > 24) isTimestamp = false;
      if(mi < 0 || mi > 60) isTimestamp = false;
    }
    return isTimestamp;
  }

  // G�r om ett Timestamp till bara YY-MM-DD
  public static String timestampToDateString(Timestamp timestamp){
    Pattern pattern = Pattern.compile("(.{4}-.{2}-.{2}).*");
    Matcher matcher = pattern.matcher(timestamp.toString());
    String outString = "";
    matcher.find();
    outString = matcher.group(1);
    return outString;
  }
  
  // G�r om en Timestamp string till bara YY-MM-DD
  public static String timestampToDateString(String timestamp){
    Pattern pattern = Pattern.compile("(.{4}-.{2}-.{2}).*");
    Matcher matcher = pattern.matcher(timestamp);
    matcher.find();
    return matcher.group(1);
  }
  
  // Checkar om det valda Timestampets dags datum finns i datorbasen
  public static boolean doesDateExist(Statement statement, Timestamp timestamp) throws SQLException{
    ResultSet resultSet = statement.executeQuery("SELECT * FROM flight");
    String dateToCheck = timestampToDateString(timestamp);
    while (resultSet.next()) {
      String outString = resultSet.getString(5);
      if(dateToCheck.equals(timestampToDateString(outString))) return true;
    }
    return false;
  }
}
