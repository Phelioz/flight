package containers;

import java.util.ArrayList;

public class Airline extends Container {
  private String name;
  private ArrayList<String> airlineFlights;

  public Airline() {
    airlineFlights = new ArrayList<String>();
  }

  public Airline(String name) {
    this.name = name;
  }

  public Airline(String name, ArrayList<String> airlineFlights) {
    this.name = name;
    this.airlineFlights = airlineFlights;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void addAirlineFlights(String flightNum) {
    airlineFlights.add(flightNum);
  }

  public ArrayList<String> getAirlineFlights() {
    return airlineFlights;
  }

  public void setAirlineFlights(ArrayList<String> airlineFlights) {
    this.airlineFlights = airlineFlights;
  }
}
